const path = require("path");
const BundleAnalyzerPlugin = require("webpack-bundle-analyzer")
	.BundleAnalyzerPlugin;
const CopyWebpackPlugin = require("copy-webpack-plugin");
const HtmlWebpackPlugin = require("html-webpack-plugin");
const OpenUI5Plugin = require("openui5-webpack-plugin");

const appPath = path.resolve(__dirname, "webapp");
const buildPath = path.resolve(__dirname, "dist");

const rootPaths = [
	"node_modules/@openui5/sap.m/src",
	"node_modules/@openui5/sap.ui.core/src",
	"node_modules/@openui5/sap.ui.core/src/sap/ui/thirdparty", // workaround for signals dependency in hasher
	"node_modules/@openui5/sap.ui.support/src",
	"node_modules/@openui5/themelib_sap_belize/src",
	"node_modules"
];

module.exports = {
	context: appPath,
	entry: "./app.js",
	mode: "development",
	module: {
		rules: [
			{
				test: /\.js$/,
				use: "babel-loader",
				exclude: /node_modules/
			},
			{
				test: /@openui5[/\\].*\.js$/,
				use: "openui5-renderer-loader"
			},
			{
				test: [
					/jquery-ui-position.js$/,
					// The following has to be fixed in the UI5 core
					/includeStylesheet.js$/
				],
				use: {
					loader: "imports-loader",
					query: "jQuery=sap/ui/thirdparty/jquery"
				}
			},
			{
				test: /jquery-mobile-custom.js$/,
				use: {
					loader: "imports-loader",
					query: "this=>window"
				}
			},
			{
				test: [/unorm\.js$/, /unormdata\.js$/],
				use: "script-loader"
			},
			{
				test: /\.xml$/,
				use: "openui5-xml-loader"
			},
			{
				test: /\.properties$/,
				use: "raw-loader"
			}
		]
	},
	output: {
		path: path.resolve(buildPath)
	},
	resolve: {
		modules: rootPaths
	},
	plugins: [
		new HtmlWebpackPlugin({
			chunksSortMode: "none",
			template: "index.html"
		}),
		new OpenUI5Plugin({
			modulePath: "sap/ui/demo/todo",
			rootPaths,
			libs: ["sap.ui.core", "sap.m"],
			translations: ["en", "de"],
			theme: ["sap_belize", "sap_belize_plus"],
			requireSync: [
				"sap/ui/core/library",
				"sap/ui/model/json/JSONModel",
				"sap/ui/model/resource/ResourceModel"
			]
		}),
		new CopyWebpackPlugin([
			{
				from: "model/todoitems.json",
				to: "sap/ui/demo/todo/model"
			},
			{
				from: "css/styles.css",
				to: "sap/ui/demo/todo/css"
			},
			{
				context: path.resolve(
					__dirname,
					"node_modules/@openui5/sap.ui.core/src"
				),
				from: {
					glob: "sap/ui/core/themes/base/fonts/**"
				}
			},
			{
				context: path.resolve(
					__dirname,
					"node_modules/@openui5/themelib_sap_belize/src"
				),
				from: {
					glob: "sap/ui/core/themes/sap_belize_plus/fonts/**"
				}
			}
		])
		/* new BundleAnalyzerPlugin({
			analyzerMode: 'static',
			generateStatsFile: true,
			openAnalyzer: false,
		})*/
	],
	devtool: "source-map"
};
